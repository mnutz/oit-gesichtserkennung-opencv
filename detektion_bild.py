import cv2

imageName = 'example.png'
image = cv2.imread(imageName)

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

cascade = 'classifiers/haarcascade_frontalface_default.xml'
detector = cv2.CascadeClassifier(cascade)

faces = detector.detectMultiScale(
    image=gray,
    scaleFactor=1.1,
    minNeighbors=5,
    minSize=(30, 30),
    flags=cv2.CASCADE_SCALE_IMAGE
)

for (x, y, w, h) in faces:
    cv2.rectangle(image, (x, y), (x+w, y+h), (255, 0, 0), 2)


cv2.imshow('Face detection', image)
cv2.waitKey(0)
cv2.destroyAllWindows()
