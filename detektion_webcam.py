import cv2

camera = cv2.VideoCapture(0)
camera.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

cascade = 'classifiers/haarcascade_frontalface_default.xml'
detector = cv2.CascadeClassifier(cascade)

while True:
    ret, image = camera.read()

    if not ret:
        print('Failed to load frame from camera!')
        break

    grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    faces = detector.detectMultiScale(
        image=grey,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    for (x, y, w, h) in faces:
        cv2.rectangle(image, (x, y), (x+w, y+h), (255, 0, 0), 2)

    cv2.imshow('Face detection', image)

    # Beende das Programm, wenn Q gedrückt wird.
    if cv2.waitKey(50) == ord('q'):
        break

camera.release()
cv2.destroyAllWindows()
