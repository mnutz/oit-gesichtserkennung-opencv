import cv2


def makedirs(directory):
    from os import makedirs
    try:
        makedirs(directory)
    except FileExistsError:
        pass


camera = cv2.VideoCapture(0)
camera.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

cascade = 'classifiers/haarcascade_frontalface_default.xml'
detector = cv2.CascadeClassifier(cascade)

face_name = input('\nEnter user name: ')
print(f'Initializing face capture for user \'{face_name}\'.')
print('Look at the camera and wait ...')

count = 0
while(True):
    ret, image = camera.read()
    if not ret:
        print('Failed to load frame from camera!')
        break

    grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = detector.detectMultiScale(
        image=grey,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    for (x, y, w, h) in faces:
        cv2.rectangle(image, (x, y), (x+w, y+h), (255, 0, 0), 2)
        count += 1

        directory = f'dataset/{face_name}/'
        makedirs(directory)
        filename = f'{directory}{count}.jpg'
        cv2.imwrite(filename, grey[y:y+h, x:x+w])

    cv2.imshow('Detected', image)

    # Beende das Programm, wenn Q gedrückt wird.
    if cv2.waitKey(50) == ord('q'):
        break
    # Oder wenn 30 Bilder gemacht wurden
    if count >= 30:
        break

print('Recording finished')
camera.release()
cv2.destroyAllWindows()
