import cv2
import os
import numpy
import json


faces = []
labels = []
label_names = []

# Finde alle Verzeichnisse im Datenverzeichnis
dirs = os.listdir('dataset')
print('Training datasets: ' + str(dirs))

label = 0

# Gehe Verzeichnisse durch
for dir_name in dirs:
    dir_path = f'dataset/{dir_name}/'

    # Finde alle Bilder in einem Verzeichnis
    files = os.listdir(dir_path)

    for file in files:
        file_path = dir_path + file
        image = cv2.imread(file_path, cv2.IMREAD_GRAYSCALE)
        faces.append(image)
        labels.append(label)

    label += 1
    label_names.append(dir_name)

recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.train(faces, numpy.array(labels))
recognizer.write('classifiers/faces.xml')
with open('classifiers/faces.json', 'w+') as of:
    json.dump(label_names, of)
