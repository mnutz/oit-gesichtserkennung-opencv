import cv2
import numpy as np
import os
import json

cascade = 'classifiers/haarcascade_frontalface_default.xml'
detector = cv2.CascadeClassifier(cascade)

recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read('classifiers/faces.xml')

font = cv2.FONT_HERSHEY_SIMPLEX

with open('classifiers/faces.json', 'r') as f:
    names = json.load(f)

camera = cv2.VideoCapture(0)
camera.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

while True:
    ret, image = camera.read()

    grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    faces = detector.detectMultiScale(
        image=grey,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    for (x, y, w, h) in faces:
        cv2.rectangle(image, (x, y), (x+w, y+h), (255, 0, 0), 2)

        pred, conf = recognizer.predict(grey[y:y+h, x:x+w])
        name = names[pred]
        confidence = f'{conf:.2f}'

        size = w / 250
        text_size, baseline = cv2.getTextSize('A TEXT', font, size, 1)
        width, height = text_size
        color = (255, 255, 255)

        if conf < 100:
            cv2.putText(image, confidence, (x+5, y+h-5), font, size, color, 2)
        else:
            name = 'unknown'
        cv2.putText(image, name, (x+5, y+height+5), font, size, color, 2)

    cv2.imshow('Face recognition', image)

    # Beende das Programm, wenn Q gedrückt wird.
    if cv2.waitKey(50) == ord('q'):
        break

camera.release()
cv2.destroyAllWindows()
