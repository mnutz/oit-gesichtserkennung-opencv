## Environment erstellen

    conda create -n oit-opencv --no-default-packages python=3.7.2
    conda activate oit-opencv

## Installation von OpenCV für Python

    pip install opencv-python
    pip install opencv-contrib-python